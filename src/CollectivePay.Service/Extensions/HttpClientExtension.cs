﻿using AspNetCore.Http.Extensions;
using CollectivePay.Config.Model;
using CollectivePay.Service.Model;
using CollectivePay.Service.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
namespace CollectivePay.Service.Extensions
{
    public static class HttpClientExtension
    {
        /// <summary>
        /// 微信Post请求 ， 已经添加了请求头信息等
        /// </summary>
        /// <param name="config">配置信息</param>
        /// <param name="requestData">请求数据</param>
        /// <returns></returns>
        public async static Task<JObject> WeChatPostAsync(this HttpClient client, WeChatPayConfig config, object requestData)
        {
            var token = BuildToken(config.url, "POST", JsonConvert.SerializeObject(requestData), config);

            client.DefaultRequestHeaders.Add("Wechatpay-Serial", config.CertificateSerialNo);
            //设置签名文档 https://pay.weixin.qq.com/wiki/doc/apiv3/wechatpay/wechatpay4_0.shtml
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("WECHATPAY2-SHA256-RSA2048", token);
            client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue("Unknown")));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync(config.url, requestData);
            var jobect = await responseMessage.Content.ReadAsJsonAsync<JObject>();

            return jobect;


        }

        /// <summary>
        /// 微信Get请求 ， 已经添加了请求头信息等
        /// </summary>
        /// <param name="config">配置信息</param>
        /// <returns></returns>
        public async static Task<JObject> WeChatGetAsync(this HttpClient client, WeChatPayConfig config)
        {
            var token = BuildToken(config.url, "GET", null, config);

            client.DefaultRequestHeaders.Add("Wechatpay-Serial", config.CertificateSerialNo);
            //设置签名文档 https://pay.weixin.qq.com/wiki/doc/apiv3/wechatpay/wechatpay4_0.shtml
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("WECHATPAY2-SHA256-RSA2048", token);
            client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue("Unknown")));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage responseMessage = await client.GetAsync(config.url);
            var jobect = await responseMessage.Content.ReadAsJsonAsync<JObject>();
            return jobect;
        }




        static string BuildToken(string url, string method, string body, WeChatPayConfig config)
        {
            var uri = new Uri(url).PathAndQuery;
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
            var nonce = Guid.NewGuid().ToString("N");
            var message = BuildMessage(method, uri, timestamp, nonce, body);
            var signature = SHA256WithRSA.Sign(config.CertificateRSAPrivateKey, message);
            return $"mchid=\"{config.mchid}\",nonce_str=\"{nonce}\",timestamp=\"{timestamp}\",serial_no=\"{config.CertificateSerialNo}\",signature=\"{signature}\"";
        }

        static string BuildMessage(string method, string uri, string timestamp, string nonce, string body)
        {
            return $"{method}\n{uri}\n{timestamp}\n{nonce}\n{body}\n";
        }
    }
}
