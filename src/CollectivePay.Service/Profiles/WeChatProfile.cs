﻿using AutoMapper;
using CollectivePay.Service.Model;
using CollectivePay.Service.Model.WeChat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectivePay.Service.Profiles
{
    public class WeChatProfile : Profile
    {
        public WeChatProfile()
        {
            CreateMap<WeChatH5OrderInfo, WeChatH5RequestData>();
        }
    }
}
