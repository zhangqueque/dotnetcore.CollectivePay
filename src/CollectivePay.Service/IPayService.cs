﻿using CollectivePay.Config.Model;
using CollectivePay.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectivePay.Service
{
    public interface IPayService<TConfig>
    {
        /// <summary>
        /// 关闭订单
        /// </summary>
        /// <param name="out_trade_no">商户系统内部订单号</param>
        /// <returns></returns>
        Task<object> CloseOrderAsync(WeChatPayConfig config, string out_trade_no);
        /// <summary>
        /// 调用支付的请求数据
        /// </summary>
        /// <param name="config"></param>
        /// <param name="orderInfo"></param>
        /// <returns></returns>
        IPayRequestData GetPayRequestData(TConfig config, IPayOrderInfo orderInfo);
        /// <summary>
        /// H5支付链接
        /// </summary>
        /// <param name="config">配置信息</param>
        /// <param name="requestData">请求数据</param>
        /// <returns></returns>
        Task<string> H5PayAsync(TConfig config, IPayRequestData requestData);
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="config">配置信息</param>
        /// <param name="out_trade_no">单号</param>
        /// <returns></returns>
        Task<object> OrderQueryAsync(WeChatPayConfig config,   string out_trade_no);
    }


}
