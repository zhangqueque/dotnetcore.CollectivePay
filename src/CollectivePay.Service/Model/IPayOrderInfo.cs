﻿using CollectivePay.Config.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectivePay.Service.Model
{
    public interface IPayOrderInfo
    {

        /// <summary>
        /// （非必填）配置的商户名称  
        /// </summary>
        string merchantName { get; set; }
        /// <summary>
        /// （必填）支付方式
        /// </summary>
        PayType payType { get; set; }
    }
}
