﻿using CollectivePay.Config.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectivePay.Service.Model.WeChat
{
    /// <summary>
    ///微信下单信息  参数信息可以查看文档： https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_3_2.shtml
    /// </summary>
    public class WeChatH5OrderInfo: IPayOrderInfo
    {

        /// <summary>
        /// （非必填）配置的商户名称  
        /// </summary>
        public string merchantName { get; set; }
        /// <summary>
        /// （必填）支付方式
        /// </summary>
        public PayType payType { get; set; }

        #region 必填
        /// <summary>
        /// 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一 示例值：1217752501201407033233368018
        /// </summary>
        public string out_trade_no { get; set; }
        /// <summary>
        /// 商品描述 示例值：Image形象店-深圳腾大-QQ公仔
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 订单金额信息
        /// </summary>
        public Amount amount { get; set; }
        /// <summary>
        /// 支付场景描述
        /// </summary>
        public Scene_Info scene_info { get; set; }
        #endregion
    }


    /// <summary>
    /// 订单金额信息
    /// </summary>
    public class Amount
    {
        /// <summary>
        /// 订单总金额，单位为分。示例值：100
        /// </summary>
        public int total { get; set; }
        /// <summary>
        /// CNY：人民币，境内商户号仅支持人民币。示例值：CNY
        /// </summary>
        public string currency { get; set; } = "CNY";
    }

    /// <summary>
    /// 支付场景描述
    /// </summary>
    public class Scene_Info
    {
        /// <summary>
        /// 用户的客户端IP，支持IPv4和IPv6两种格式的IP地址。 示例值：14.23.150.211
        /// </summary>
        public string payer_client_ip { get; set; }
        /// <summary>
        /// H5场景信息
        /// </summary>
        public H5_Info h5_info { get; set; }
    }
    /// <summary>
    /// H5场景信息
    /// </summary>
    public class H5_Info
    {
        /// <summary>
        /// 场景类型 示例值：iOS, Android, Wap
        /// </summary>
        public string type { get; set; } = "Wap";
    }

}
