﻿using CollectivePay.Service.Model;
using CollectivePay.Service.Model.WeChat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectivePay.Service.Model 
{
    /// <summary>
    ///微信H5支付请求信息
    /// </summary>
    public class WeChatH5RequestData: IPayRequestData
    {
        
        /// <summary>
        /// 直连商户的商户号，由微信支付生成并下发。示例值：1230000109
        /// </summary>
        public string mchid { get; set; }
        /// <summary>
        /// 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一 示例值：1217752501201407033233368018
        /// </summary>
        public string out_trade_no { get; set; }
        /// <summary>
        /// 由微信生成的应用ID，全局唯一。请求基础下单接口时请注意APPID的应用属性，应为公众号的APPID 示例值：wxd678efh567hg6787
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 商品描述 示例值：Image形象店-深圳腾大-QQ公仔
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 通知URL必须为直接可访问的URL，不允许携带查询串，要求必须为https地址。格式：URL 示例值：https://www.weixin.qq.com/wxpay/pay.php
        /// </summary>
        public string notify_url { get; set; }
        /// <summary>
        /// 订单金额信息
        /// </summary>
        public Amount amount { get; set; }
        /// <summary>
        /// 支付场景描述
        /// </summary>
        public Scene_Info scene_info { get; set; }
    }


    ///// <summary>
    ///// 订单金额信息
    ///// </summary>
    //public class Amount
    //{
    //    /// <summary>
    //    /// 订单总金额，单位为分。示例值：100
    //    /// </summary>
    //    public int total { get; set; }
    //    /// <summary>
    //    /// CNY：人民币，境内商户号仅支持人民币。示例值：CNY
    //    /// </summary>
    //    public string currency { get; set; } = "CNY";
    //}

    ///// <summary>
    ///// 支付场景描述
    ///// </summary>
    //public class Scene_Info
    //{
    //    /// <summary>
    //    /// 用户的客户端IP，支持IPv4和IPv6两种格式的IP地址。 示例值：14.23.150.211
    //    /// </summary>
    //    public string payer_client_ip { get; set; }
    //    /// <summary>
    //    /// H5场景信息
    //    /// </summary>
    //    public H5_Info h5_info { get; set; }
    //}
    ///// <summary>
    ///// H5场景信息
    ///// </summary>
    //public class H5_Info
    //{
    //    /// <summary>
    //    /// 场景类型 示例值：iOS, Android, Wa
    //    /// </summary>
    //    public string type { get; set; } = "Wap";
    //}

}
