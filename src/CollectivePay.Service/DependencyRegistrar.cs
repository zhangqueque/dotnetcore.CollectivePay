﻿
using CollectivePay.Config;
using CollectivePay.Config.Model;
using CollectivePay.Service;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
 
namespace Microsoft.Extensions.DependencyInjection
{
    public static class DependencyRegistrar
    {
        public static IServiceCollection AddWeChatPay(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddSingleton<IConfigProvider<WeChatPayConfig>, WeChatConfigProvider>();
            services.AddSingleton<IPayService<WeChatPayConfig>, WeChatPayService>();

            return services;
        }
    }
}
