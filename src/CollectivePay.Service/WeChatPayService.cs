﻿using AutoMapper;
using CollectivePay.Config;
using CollectivePay.Config.Enums;
using CollectivePay.Config.Model;
using CollectivePay.Service.Extensions;
using CollectivePay.Service.Model;
using CollectivePay.Service.Model.WeChat;
using CollectivePay.Service.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CollectivePay.Service
{
    public class WeChatPayService : IPayService<WeChatPayConfig>
    {
        private readonly IConfigProvider<WeChatPayConfig> _configProvider;
        private readonly IMapper _mapper;

        public WeChatPayService(IConfigProvider<WeChatPayConfig> configProvider, IMapper mapper)
        {
            _configProvider = configProvider;
            _mapper = mapper;
        }


        public IPayRequestData GetPayRequestData(WeChatPayConfig config, IPayOrderInfo orderInfo)
        {
            switch (orderInfo.payType)
            {
                case PayType.WeChatH5:
                    WeChatH5RequestData requestData = _mapper.Map<WeChatH5RequestData>(orderInfo as WeChatH5OrderInfo);
                    requestData.appid = config.appid;
                    requestData.mchid = config.mchid;
                    requestData.notify_url = config.notify_url;
                    return requestData;
                default:
                    return null;
            }
        }

        /// <summary>
        /// H5支付链接
        /// </summary>
        /// <param name="config">配置信息</param>
        /// <param name="requestData">请求数据</param>
        /// <returns></returns>
        public async Task<string> H5PayAsync(WeChatPayConfig config, IPayRequestData requestData)
        {
            config.url = "https://api.mch.weixin.qq.com/v3/pay/transactions/h5";
            HttpClient httpClient = new HttpClient();
            var jobject = await httpClient.WeChatPostAsync(config, requestData);
            return jobject["h5_url"].ToString();
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="config">配置信息</param>
        /// <param name="out_trade_no">单号</param>
        /// <returns></returns>
        public async Task<object> OrderQueryAsync(WeChatPayConfig config, string out_trade_no)
        {
            Regex regex = new Regex(@"^\d+$");
            if (regex.IsMatch(out_trade_no))
            {
                //微信支付订单号查询
                config.url = $"https://api.mch.weixin.qq.com/v3/pay/transactions/id/{out_trade_no}?mchid={config.mchid}";
            }
            else
            {
                //商户订单号查询
                config.url = $"https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/{out_trade_no}?mchid={config.mchid}";
            }
            HttpClient httpClient = new HttpClient();
            var jobject = await httpClient.WeChatGetAsync(config);
            return jobject;
        }

        /// <summary>
        /// 关闭订单
        /// </summary>
        /// <param name="out_trade_no">商户系统内部订单号</param>
        /// <returns></returns>
        public async Task<object> CloseOrderAsync(WeChatPayConfig config, string out_trade_no)
        {
            config.url = $"https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/{out_trade_no}/close";
            HttpClient httpClient = new HttpClient();
            var jobject = await httpClient.WeChatPostAsync(config, new { mchid = config.mchid });
            return jobject;
        }



    }
}
