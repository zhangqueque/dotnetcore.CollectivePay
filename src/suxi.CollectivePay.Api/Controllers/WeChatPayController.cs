﻿using CollectivePay.Config;
using CollectivePay.Config.Enums;
using CollectivePay.Config.Model;
using CollectivePay.Service;
using CollectivePay.Service.Model.WeChat;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectivePay.Api.Samples.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeChatPayController : ControllerBase
    {
        /// <summary>
        /// 获取H5支付的跳转链接
        /// </summary>
        /// <param name="weChatOrderInfo">订单信息</param>
        /// <returns></returns>
        [HttpPost("h5")]
        public async Task<IActionResult> WeChatH5PayAsync([FromServices] IConfigProvider<WeChatPayConfig> configProvider, [FromServices] IPayService<WeChatPayConfig> payService, [FromBody] WeChatH5OrderInfo weChatOrderInfo)
        {

            var config = configProvider.GetConfig(weChatOrderInfo.payType, weChatOrderInfo.merchantName);
            var data = payService.GetPayRequestData(config, weChatOrderInfo);
            var str = await payService.H5PayAsync(config,data);
 
            return Ok(str);
        }
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="out_trade_no">微信订单号 / 商户自定义订单号</param>
        /// <param name="merchantName">商户名称</param>
        /// <returns></returns>
        [HttpGet("query")]
        public async Task<IActionResult> QueryOrderAsync([FromServices] IConfigProvider<WeChatPayConfig> configProvider, [FromServices] IPayService<WeChatPayConfig> payService,string merchantName,  string out_trade_no)
        {
            var config = configProvider.GetConfig(PayType.WeChatH5, merchantName);
            var jobject = await payService.OrderQueryAsync(config,  out_trade_no);
            return Ok(jobject);
        }
        /// <summary>
        /// 关闭订单
        /// </summary>
        /// <param name="merchantName">商户名称</param>
        /// <param name="out_trade_no">商户自定义订单号</param>
        /// <returns></returns>
        [HttpGet("close")]
        public async Task<IActionResult> CloseOrderAsync([FromServices] IConfigProvider<WeChatPayConfig> configProvider, [FromServices] IPayService<WeChatPayConfig> payService,string merchantName, string out_trade_no)
        {
            var config = configProvider.GetConfig(PayType.WeChatH5, merchantName);
            var jobject = await payService.CloseOrderAsync(config, out_trade_no);
            return Ok(jobject);
        }
        
    }
}
