using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace suxi.CollectivePay.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {

                c.SwaggerDoc(Configuration["paySwagger:name"], new OpenApiInfo
                {
                    Title = Configuration["paySwagger:title"],
                    Version = Configuration["paySwagger:version"],
                    Description = Configuration["paySwagger:description"],
                    Contact = new OpenApiContact
                    {
                        Name = Configuration["paySwagger:contactName"],
                        Email = Configuration["paySwagger:contactEmail"]
                    }
                });
#if DEBUG
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "suxi.CollectivePay.Api.xml"));
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "CollectivePay.Service.xml"));
#else
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "suxi.CollectivePay.Api.xml"));
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "CollectivePay.Service.xml"));
#endif
            });
            services.AddCors(options => options.AddPolicy("cors", builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()));
            services.AddWeChatPay(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint(string.Format("/swagger/{0}/swagger.json", Configuration["paySwagger:name"]), "suxi.CollectiveOAuth.Api v1"));

            app.UseHttpsRedirection();
            app.UseCors("cors");

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
