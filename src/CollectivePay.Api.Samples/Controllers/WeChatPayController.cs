﻿using CollectivePay.Config;
using CollectivePay.Config.Model;
using CollectivePay.Service;
using CollectivePay.Service.Model.WeChat;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectivePay.Api.Samples.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeChatPayController : ControllerBase
    {
        [HttpPost("h5")]
        public async Task<IActionResult> Post([FromServices] IConfigProvider<WeChatPayConfig> configProvider, [FromServices] IPayService<WeChatPayConfig> payService, [FromBody] WeChatH5OrderInfo weChatOrderInfo)
        {

            var config = configProvider.GetConfig(weChatOrderInfo.payType, weChatOrderInfo.merchantName);
            var data = payService.GetPayRequest(config, weChatOrderInfo);
            var str = await payService.WeChatH5Pay(config,data);
 
            return Ok(str);
        }
    }
}
