﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace CollectivePay.Config.Model
{
    /// <summary>
    /// 请求微信支付接口的配置信息
    /// </summary>
    public class WeChatPayConfig
    {
        /// <summary>
        /// 本次要请求的地址
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// (必填) 由微信生成的应用ID，全局唯一。请求基础下单接口时请注意APPID的应用属性，应为公众号的APPID 示例值：wxd678efh567hg6787
        /// </summary>
        public string appid { get; set; }

        /// <summary>
        /// (必填)直连商户的商户号，由微信支付生成并下发。示例值：1230000109
        /// </summary>
        public string mchid { get; set; }

        /// <summary>
        /// (必填) 通知URL必须为直接可访问的URL，不允许携带查询串，要求必须为https地址。格式：URL 示例值：https://www.weixin.qq.com/wechat/callback
        /// </summary>
        public string notify_url { get; set; }

 
        public string certificatePassword;

        /// <summary>
        /// (非必填，证书不设置密码，默认就是商户号)API证书密码(默认为商户号)
        /// </summary>
        public string CertificatePassword
        {
            get => string.IsNullOrEmpty(certificatePassword) ? mchid : certificatePassword;
            set => certificatePassword = value;
        }


        private string certificatePath;
        /// <summary>
        /// (必填)证书文件路径/证书文件的base64字符串
        /// set 里给 
        /// </summary>
        public string CertificatePath
        {
            get { return certificatePath; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {

                    certificatePath = value;
                    //参考文档 https://docs.microsoft.com/zh-cn/dotnet/api/system.security.cryptography.x509certificates.x509certificate2?view=net-5.0
                    //https://pay.weixin.qq.com/wiki/doc/apiv3/wechatpay/wechatpay3_0.shtml 等会调取微信接口时需要 证书序列号 和 私钥
                    X509Certificate2 = File.Exists(value) ? new X509Certificate2(value, CertificatePassword)
                        : Base64Util.IsBase64String(value) ? new X509Certificate2(Convert.FromBase64String(value), CertificatePassword)
                        : throw new Exception("证书文件不存在或证书的Base64String不正确！");

                    CertificateSerialNo = X509Certificate2.GetSerialNumberString();
                    CertificateRSAPrivateKey = X509Certificate2.GetRSAPrivateKey();
                }
            }
        }

        #region 给证书路径赋值后，自动赋值完成
        /// <summary>
        /// X.509证书
        /// </summary>
        public X509Certificate2 X509Certificate2 { get; private set; }
        /// <summary>
        /// 私钥，如果证书没有RSA私钥，则为空。  
        /// </summary>
        public RSA CertificateRSAPrivateKey { get; private set; }

        /// <summary>
        /// X.509证书的序列号。
        /// </summary>
        public string CertificateSerialNo { get; private set; }
        #endregion


        public static class Base64Util
        {
            /// <summary>
            ///验证是否时base64字符串
            /// </summary>
            /// <param name="str">内容</param>
            /// <returns></returns>
            public static bool IsBase64String(string str)
            {
                Span<byte> buffer = stackalloc byte[str.Length];
                return Convert.TryFromBase64String(str, buffer, out _);
            }
        }
    }
}
