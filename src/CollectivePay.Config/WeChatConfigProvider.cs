﻿using CollectivePay.Config.Enums;
using CollectivePay.Config.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectivePay.Config
{
    public class WeChatConfigProvider : IConfigProvider<WeChatPayConfig>
    {
        private readonly IConfiguration _configuration;

        public WeChatConfigProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public WeChatPayConfig GetConfig(PayType payType, string merchantName = "Default")
        {
            //获取配置文件信息的方式，自己实现，就是给WeChatPayConfig里的必填项赋值

            WeChatPayConfig config = new WeChatPayConfig();
            switch (payType)
            {

                case PayType.WeChatH5:
                    
                    config.appid = _configuration[$"Pay_{merchantName}_{PayType.WeChatH5.ToString()}_Appid"];
                    config.mchid = _configuration[$"Pay_{merchantName}_{PayType.WeChatH5.ToString()}_Mchid"];
                    config.notify_url = _configuration[$"Pay_{merchantName}_{PayType.WeChatH5.ToString()}_NotifyUrl"];
                    config.CertificatePath = _configuration[$"Pay_{merchantName}_{PayType.WeChatH5.ToString()}_CertificatePath"];
                    return config;

                default:
                    return null;
            }

        }

    
    }
}
