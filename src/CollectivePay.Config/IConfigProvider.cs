﻿using CollectivePay.Config.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectivePay.Config
{
    public interface IConfigProvider<T>
    {
        /// <summary>
        ///获取商户的配置信息
        /// </summary>
        /// <typeparam name="T">返回的配置类</typeparam>
        /// <param name="payType">支付类型</param>
        /// <param name="merchantName">商户名称（用于获取商户对应的Config，如果只有一个商户使用，不用传）</param>
        /// <returns></returns>
        T GetConfig(PayType payType, string merchantName="Default");

        
    }
}
